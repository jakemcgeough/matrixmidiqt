![matrixMIDIqt](matrixMIDIpic.png)

MatrixMIDIqt enables intuitive control of the Oberheim Matrix 1000, an analog synthesizer which is difficult to program due to its limited interface.

This MIDI Controller allows the user to control all parameters, including modulation matrix, via an intuitive graphical user interface.

Written for Windows using C++ and Qt.

