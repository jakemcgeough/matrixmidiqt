#include <QApplication>
#include <cstdlib>
#include <string>
#include <vector>

#include "mainwindow.h"
#include "mididevices.h"

using namespace std;

int main(int argc, char *argv[]) {

    MidiDevices midiDevices = MidiDevices();

    // pass midiDevices pointer to MainWindow

    QApplication a(argc, argv);
    a.setStyle("fusion");
    MainWindow w(&midiDevices);
    w.setWindowTitle("Matrix MIDI");
    w.setWindowState(Qt::WindowMaximized);
    w.show();
    return a.exec();
}

