#include "comboboxlabel.h"

comboBoxLabel::comboBoxLabel(QString title, int param)
    : param(param)
{
    label.setText(title);

    layout.addWidget(&cb);
    layout.addWidget(&label);

    layout.setAlignment(&cb, Qt::AlignCenter);
    layout.setAlignment(&label, Qt::AlignCenter);

    setLayout(&layout);

    connect(&cb, &QComboBox::currentIndexChanged, this, &comboBoxLabel::changeIndex);
}

void comboBoxLabel::addItems(QStringList items) {
    cb.addItems(items);
}

void comboBoxLabel::changeIndex(int value) {
    emit currentIndexChanged(value);
}

int comboBoxLabel::GetParam() const {
    return param;
}
