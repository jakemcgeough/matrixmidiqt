#include "dial.h"

dial::dial(const QString title, int min, int max, int param)
    : text(title),
      param(param),
      arcRect(new QRectF),
      valueRect(new QRectF),
      textRect(new QRectF),
      arcColor(new QColor),
      arcPen(new QPen)
{
    setMinimum(min);
    setMaximum(max);
    setSingleStep(1);
    setPageStep(1);
    setTracking(true);

    // update numerical display and angle of blue fill
    connect(this, &QDial::valueChanged, this, &dial::updateValue);

//    setMinimumSize(100, 100);
    setFixedSize(100, 100);
    SetMaximumAngle(-270);
    SetStartAngle(225);

    SetArcWidth(3);
    SetArcColor("blue");

    // test:
//    setStyleSheet({"background-color: red;"});

    updateValue();
}

dial::~dial() = default;

void dial::mousePressEvent(QMouseEvent *event) {
    mousePressPoint = event->pos();
    dragging = true;
    baseValue = value();
}

void dial::mouseReleaseEvent(QMouseEvent *event) {
    dragging = false;
}

void dial::mouseMoveEvent(QMouseEvent *event) {
    if (dragging) {
        int new_value = baseValue + scaleFactor * (mousePressPoint.y() - event->y());
        setValue(new_value);
    }
}

void dial::paintEvent(QPaintEvent*) {
    QPainter painter(this);

    // So that we can use the background color
    // Otherwise the background is transparent
    painter.setBackgroundMode(Qt::OpaqueMode);

    QBrush brush = QBrush("#161616");
    painter.setBackground(brush);

    // Smooth out the circle
    painter.setRenderHint(QPainter::Antialiasing);

    // Use background color
    painter.setBrush(painter.background());

    // red stuff for testing:
//    QBrush red = QBrush("red");
//    painter.setBrush(red);
//    painter.fillRect(*textRect, red);
//    painter.fillRect(*arcRect, red);
//    painter.fillRect(*textRect, red);

    // Get current pen before resetting so we have
    // access to the color() method which returns the
    // color from the stylesheet
    QPen textPen = painter.pen();

    textPen.setColor("#c2c2c2");

    // No border
//    painter.setPen(QPen(Qt::NoPen));

    textPen.setWidth(2);
    painter.setPen(textPen);
    // Draw background circle
//    painter.drawEllipse(QDial::rect());
//    textPen.setWidth(3);
    painter.drawArc(*arcRect, startAngle, maximumAngleSpan);

    painter.drawText(*textRect, Qt::AlignHCenter | Qt::AlignTop, text);

    QFont font = painter.font();
    font.setPixelSize(16);
    painter.setFont(font);
    painter.setPen("red");
    painter.drawText(*valueRect, Qt::AlignCenter, valueString);

    painter.setPen(*arcPen);
    painter.drawArc(*arcRect, startAngle, angleSpan);
}

void dial::resizeEvent(QResizeEvent* event) {
    QDial::setMinimumSize(event->size());

    double width = QDial::width() - (2 * arcWidth);
    double height = width / 2;
//    double height = width;

    *textRect = QRectF(arcWidth, QDial::width() * 0.7, QDial::width(), height);
//    *valueRect = QRectF(arcWidth, height, width, height);
    *valueRect = QRectF(arcWidth + (QDial::width() * 0.15),
                        arcWidth,
                        (QDial::width() * 0.7 - arcWidth),
                        (QDial::width() * 0.7 - arcWidth));
//    *arcRect = QRectF(arcWidth / 2, arcWidth / 2, QDial::width() - arcWidth, QDial::height() - arcWidth);
    *arcRect = QRectF(arcWidth + (QDial::width() * 0.15),
                     arcWidth,
                     (QDial::width() * 0.7 - arcWidth),
                     (QDial::width() * 0.7 - arcWidth));
//    *arcRect = QRectF(arcWidth + (QDial::width() * 0.1),
//                     arcWidth,
//                     60,
//                     60);
}

void dial::updateValue() {
    double value = QDial::value();

    // Get ratio between current value and maximum to calculate angle
    double ratio = value / QDial::maximum();

    angleSpan = maximumAngleSpan * ratio;

    valueString = QString::number(value);
}


void dial::SetArcWidth(double px) {
    // width of the pen
    arcWidth = px;
    *arcRect = QRectF(arcWidth / 2, arcWidth / 2, QDial::width() - arcWidth, QDial::height() - arcWidth);
    arcPen->setWidth(arcWidth);
}

void dial::SetText(const QString& text) {
    this->text = text;
}

QString dial::GetText() const {
    return this->text;
}

int dial::GetParam() const {
    return param;
}

double dial::GetArcWidth() const {
    return arcWidth;
}

void dial::SetMaximumAngle(double angle) {
    maximumAngleSpan = angle * 16;
}

double dial::GetMaximumAngle() const {
    return maximumAngleSpan / 16;
}

void dial::SetStartAngle(double angle) {
    startAngle = angle * 16;
}

double dial::GetStartAngle() const {
    return startAngle / 16;
}

void dial::SetArcColor(const QString& color) {
    arcColor->setNamedColor(color);

    arcPen->setColor(*arcColor);
}

QString dial::GetArcColor() const {
    return arcColor->name();
}

















