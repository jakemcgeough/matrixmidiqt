#include "sectionBox.h"

sectionBox::sectionBox(int numColumns) : numColumns(numColumns) {
    setFrameShape(QFrame::Box);

    setLayout(&layout);

    for (int i = 0; i < numColumns; ++i) {
        layout.setColumnMinimumWidth(i, 10);
    }

    setStyleSheet("QFrame { border : 2px solid #b2b2b2; border-radius: 20px; opacity: 255; background-color: #161616 } QLabel {border : 0}");
}

void sectionBox::addWidget(QWidget *qw) {
    int column = widgetIndex;
    int row = 0;

    while (column >= numColumns) {
        column -= numColumns;
        ++row;
    }

    ++widgetIndex;

    layout.addWidget(qw, row, column);
}

