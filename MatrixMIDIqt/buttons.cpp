#include "buttons.h"

buttons::buttons(QString title, std::vector<QString> bNames, int param)
    : param(param)
{
    label.setText(title);

    numBits = bNames.size();

    // make sure the order actually corresponds to the correct bits

    for (size_t i = 0; i < bNames.size(); i++) {
        pushButton *qpb = new pushButton(bNames[i], i);
        qpb->setCheckable(true);
        qpb->setStyleSheet("QPushButton::checked {border: 1px solid blue; border-radius: 3px}");
        bNamePointers[bNames[i]] = qpb;

        layout.addWidget(qpb);
        connect(qpb, &pushButton::PressButton, this, &buttons::PressButton);
    }

    layout.addWidget(&label);
    setLayout(&layout);
}

pushButton *buttons::GetButtonByName(QString bName) {
    return bNamePointers[bName];
}

void buttons::PressButton(int currentBit) {
    if (value & (1 << currentBit)) {
        value -= (1 << currentBit);
    }
    else {
        value += (1 << currentBit);
    }

    emit updateValue(value);
}

int buttons::GetParam() const {
    return param;
}
