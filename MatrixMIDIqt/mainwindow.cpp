#include "mainwindow.h"
#include "ui_mainwindow.h"

// multiple vertical layouts - one for each synth section
// create each dial manually - name each one

MainWindow::MainWindow(MidiDevices *midiDevices) :
    ui(new Ui::MainWindow),
    midiDevices(midiDevices)
{
    ui->setupUi(this);

//    this->setStyleSheet("background-color: #161616;");

    mw = new QWidget(this);

    mw->setStyleSheet("QWidget { opacity: 255; background-color: #161616; color: #c2c2c2 } QCheckBox[checked = true] {border: 1px solid #c2c2c2}");

    setCentralWidget(mw);

    QGridLayout *mainLayout = new QGridLayout(mw);


//    mainLayout->setAlignment(Qt::);

    // set up synth sections, define dimensions and layout:
    mainLayout->addWidget(osc1, 0, 0, 28, 27);
    mainLayout->addWidget(osc2, 28, 0, 28, 27);
    mainLayout->addWidget(misc, 56, 0, 44, 27);

    mainLayout->addWidget(filterAmp, 0, 27, 40, 20);
    mainLayout->addWidget(lfo1, 40, 27, 30, 20);
    mainLayout->addWidget(lfo2, 70, 27, 30, 20);

    mainLayout->addWidget(env1, 0, 47, 25, 33);
    mainLayout->addWidget(env2, 25, 47, 25, 33);
    mainLayout->addWidget(env3, 50, 47, 25, 33);
    mainLayout->addWidget(tp, 75, 47, 25, 33);

    mainLayout->addWidget(matrix, 0, 80, 100, 20);


    // add invidual controls to synth sections:
    testMidiStuff.addItems(midiDevices->GetOutDeviceNames());
    connect(&testMidiStuff, &QComboBox::currentIndexChanged, midiDevices, &MidiDevices::SelectOutDevice);
//    osc1->addWidget(&testMidiStuff);
// where to put this? ^^

    osc1->addWidget(&osc1Freq);
    osc1->addWidget(&osc1FreqByLfo1);
    osc1->addWidget(&osc1Pw);
    osc1->addWidget(&osc1PwByLfo2);
    osc1->addWidget(&osc1Waveform);
    osc1->addWidget(&osc1Ws);
    osc1->addWidget(&osc1FixedMod1);
    osc1->addWidget(&osc1FixedMod2);

    osc2->addWidget(&osc2Freq);
    osc2->addWidget(&osc2FreqByLfo1);
    osc2->addWidget(&osc2Pw);
    osc2->addWidget(&osc2PwByLfo2);
    osc2->addWidget(&osc2Waveform);
    osc2->addWidget(&osc2Ws);
    osc2->addWidget(&osc2FixedMod1);
    osc2->addWidget(&osc2FixedMod2);

    oscSync.addItems(oscSyncCodes);
    ramp1Mode.addItems(rampModeCodes);
    ramp2Mode.addItems(rampModeCodes);
    keyboardMode.addItems(keyboardModeCodes);
    legatoMode.addItems(legatoModeCodes);

    misc->addWidget(&oscSync);
    misc->addWidget(&detune);
    misc->addWidget(&mix);
    misc->addWidget(&ramp1Rate);
    misc->addWidget(&ramp1Mode);
    misc->addWidget(&ramp2Rate);
    misc->addWidget(&ramp2Mode);
    misc->addWidget(&keyboardMode);
    misc->addWidget(&portamentoRate);
    misc->addWidget(&portamentoRateByVel);
    misc->addWidget(&legatoMode);
    misc->addWidget(&legato);

    filterAmp->addWidget(&cutoff);
    filterAmp->addWidget(&resonance);
    filterAmp->addWidget(&filterFixedMod);
    filterAmp->addWidget(&cutoffByPressure);
    filterAmp->addWidget(&cutoffByEnv1);
    filterAmp->addWidget(&cutoffByKeyboard);
    filterAmp->addWidget(&fm);
    filterAmp->addWidget(&fmByEnv3);
    filterAmp->addWidget(&fmByPressure);
    filterAmp->addWidget(&amp1Amount);
    filterAmp->addWidget(&amp1ByVel);
    filterAmp->addWidget(&amp2ByEnv2);

    lfo1Wave.addItems(lfoWaveCodes);
    lfo1Trigger.addItems(lfoTriggerCodes);
    lfo1SampledSource.addItems(matrixSourceCodes);

    lfo1->addWidget(&lfo1Speed);
    lfo1->addWidget(&lfo1SpeedByPress);
    lfo1->addWidget(&lfo1Wave);
    lfo1->addWidget(&lfo1Amplitude);
    lfo1->addWidget(&lfo1AmpByRamp1);
    lfo1->addWidget(&lfo1RetrigPoint);
    lfo1->addWidget(&lfo1Trigger);
    lfo1->addWidget(&lfo1Lag);
    lfo1->addWidget(&lfo1SampledSource);

    lfo2Wave.addItems(lfoWaveCodes);
    lfo2Trigger.addItems(lfoTriggerCodes);
    lfo2SampledSource.addItems(matrixSourceCodes);

    lfo2->addWidget(&lfo2Speed);
    lfo2->addWidget(&lfo2SpeedByPress);
    lfo2->addWidget(&lfo2Wave);
    lfo2->addWidget(&lfo2Amplitude);
    lfo2->addWidget(&lfo2AmpByRamp2);
    lfo2->addWidget(&lfo2RetrigPoint);
    lfo2->addWidget(&lfo2Trigger);
    lfo2->addWidget(&lfo2Lag);
    lfo2->addWidget(&lfo2SampledSource);

    env1->addWidget(&env1Delay);
    env1->addWidget(&env1Attack);
    env1->addWidget(&env1Decay);
    env1->addWidget(&env1Sustain);
    env1->addWidget(&env1Release);
    env1->addWidget(&env1Amplitude);
    env1->addWidget(&env1AmpByVel);
    env1->addWidget(&env1TriggerMode);
    env1->addWidget(&env1Mode);
    env1->addWidget(&env1LfoTriggerMode);

    env2->addWidget(&env2Delay);
    env2->addWidget(&env2Attack);
    env2->addWidget(&env2Decay);
    env2->addWidget(&env2Sustain);
    env2->addWidget(&env2Release);
    env2->addWidget(&env2Amplitude);
    env2->addWidget(&env2AmpByVel);
    env2->addWidget(&env2TriggerMode);
    env2->addWidget(&env2Mode);
    env2->addWidget(&env2LfoTriggerMode);

    env3->addWidget(&env3Delay);
    env3->addWidget(&env3Attack);
    env3->addWidget(&env3Decay);
    env3->addWidget(&env3Sustain);
    env3->addWidget(&env3Release);
    env3->addWidget(&env3Amplitude);
    env3->addWidget(&env3AmpByVel);
    env3->addWidget(&env3TriggerMode);
    env3->addWidget(&env3Mode);
    env3->addWidget(&env3LfoTriggerMode);

    tpSource.addItems(matrixSourceCodes);

    tp->addWidget(&tp1);
    tp->addWidget(&tp2);
    tp->addWidget(&tp3);
    tp->addWidget(&tp4);
    tp->addWidget(&tp5);
    tp->addWidget(&tpSource);

    matrixSource1.addItems(matrixSourceCodes);
    matrixSource2.addItems(matrixSourceCodes);
    matrixSource3.addItems(matrixSourceCodes);
    matrixSource4.addItems(matrixSourceCodes);
    matrixSource5.addItems(matrixSourceCodes);
    matrixSource6.addItems(matrixSourceCodes);
    matrixSource7.addItems(matrixSourceCodes);
    matrixSource8.addItems(matrixSourceCodes);
    matrixSource9.addItems(matrixSourceCodes);
    matrixSource10.addItems(matrixSourceCodes);

    matrixDestination1.addItems(matrixDestinationCodes);
    matrixDestination2.addItems(matrixDestinationCodes);
    matrixDestination3.addItems(matrixDestinationCodes);
    matrixDestination4.addItems(matrixDestinationCodes);
    matrixDestination5.addItems(matrixDestinationCodes);
    matrixDestination6.addItems(matrixDestinationCodes);
    matrixDestination7.addItems(matrixDestinationCodes);
    matrixDestination8.addItems(matrixDestinationCodes);
    matrixDestination9.addItems(matrixDestinationCodes);
    matrixDestination10.addItems(matrixDestinationCodes);

    matrix->addWidget(&matrixSource1);
    matrix->addWidget(&matrixAmount1);
    matrix->addWidget(&matrixDestination1);

    matrix->addWidget(&matrixSource2);
    matrix->addWidget(&matrixAmount2);
    matrix->addWidget(&matrixDestination2);

    matrix->addWidget(&matrixSource3);
    matrix->addWidget(&matrixAmount3);
    matrix->addWidget(&matrixDestination3);

    matrix->addWidget(&matrixSource4);
    matrix->addWidget(&matrixAmount4);
    matrix->addWidget(&matrixDestination4);

    matrix->addWidget(&matrixSource5);
    matrix->addWidget(&matrixAmount5);
    matrix->addWidget(&matrixDestination5);

    matrix->addWidget(&matrixSource6);
    matrix->addWidget(&matrixAmount6);
    matrix->addWidget(&matrixDestination6);

    matrix->addWidget(&matrixSource7);
    matrix->addWidget(&matrixAmount7);
    matrix->addWidget(&matrixDestination7);

    matrix->addWidget(&matrixSource8);
    matrix->addWidget(&matrixAmount8);
    matrix->addWidget(&matrixDestination8);

    matrix->addWidget(&matrixSource9);
    matrix->addWidget(&matrixAmount9);
    matrix->addWidget(&matrixDestination9);

    matrix->addWidget(&matrixSource10);
    matrix->addWidget(&matrixAmount10);
    matrix->addWidget(&matrixDestination10);



    // connect all dials to midiDevices in order to send messages:
    connect(&osc1Freq, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&osc1FreqByLfo1, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&osc1Pw, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&osc1PwByLfo2, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&osc1Waveform, &buttons::updateValue, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&osc1Ws, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&osc1FixedMod1, &buttons::updateValue, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&osc1FixedMod2, &buttons::updateValue, midiDevices, &MidiDevices::SendMidiMessage);

    connect(&osc2Freq, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&osc2FreqByLfo1, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&osc2Pw, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&osc2PwByLfo2, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&osc2Waveform, &buttons::updateValue, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&osc2Ws, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&osc2FixedMod1, &buttons::updateValue, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&osc2FixedMod2, &buttons::updateValue, midiDevices, &MidiDevices::SendMidiMessage);

    connect(&oscSync.cb, &QComboBox::currentIndexChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&detune, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&mix, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&ramp1Rate, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&ramp1Mode.cb, &QComboBox::currentIndexChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&ramp2Rate, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&ramp2Mode.cb, &QComboBox::currentIndexChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&keyboardMode.cb, &QComboBox::currentIndexChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&portamentoRate, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&portamentoRateByVel, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&legatoMode.cb, &QComboBox::currentIndexChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&legato, &buttons::updateValue, midiDevices, &MidiDevices::SendMidiMessage);

    connect(&cutoff, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&resonance, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&filterFixedMod, &buttons::updateValue, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&cutoffByPressure, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&cutoffByEnv1, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&cutoffByKeyboard, &buttons::updateValue, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&fm, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&fmByEnv3, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&fmByPressure, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&amp1Amount, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&amp1ByVel, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&amp2ByEnv2, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);

    connect(&lfo1Speed, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&lfo1SpeedByPress, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&lfo1Wave.cb, &QComboBox::currentIndexChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&lfo1Amplitude, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&lfo1AmpByRamp1, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&lfo1RetrigPoint, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&lfo1Trigger.cb, &QComboBox::currentIndexChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&lfo1Lag,  &buttons::updateValue, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&lfo1SampledSource.cb, &QComboBox::currentIndexChanged, midiDevices, &MidiDevices::SendMidiMessage);

    connect(&lfo2Speed, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&lfo2SpeedByPress, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&lfo2Wave.cb, &QComboBox::currentIndexChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&lfo2Amplitude, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&lfo2AmpByRamp2, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&lfo2RetrigPoint, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&lfo2Trigger.cb, &QComboBox::currentIndexChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&lfo2Lag, &buttons::updateValue, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&lfo2SampledSource.cb, &QComboBox::currentIndexChanged, midiDevices, &MidiDevices::SendMidiMessage);

    connect(&env1Delay, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&env1Attack, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&env1Decay, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&env1Sustain, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&env1Release, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&env1Amplitude, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&env1AmpByVel, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&env1TriggerMode, &buttons::updateValue, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&env1Mode, &buttons::updateValue, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&env1LfoTriggerMode, &buttons::updateValue, midiDevices, &MidiDevices::SendMidiMessage);

    connect(&env2Delay, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&env2Attack, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&env2Decay, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&env2Sustain, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&env2Release, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&env2Amplitude, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&env2AmpByVel, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&env2TriggerMode, &buttons::updateValue, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&env2Mode, &buttons::updateValue, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&env2LfoTriggerMode, &buttons::updateValue, midiDevices, &MidiDevices::SendMidiMessage);

    connect(&env3Delay, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&env3Attack, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&env3Decay, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&env3Sustain, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&env3Release, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&env3Amplitude, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&env3AmpByVel, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&env3TriggerMode, &buttons::updateValue, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&env3Mode, &buttons::updateValue, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&env3LfoTriggerMode, &buttons::updateValue, midiDevices, &MidiDevices::SendMidiMessage);

    connect(&tp1, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&tp2, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&tp3, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&tp4, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&tp5, &QDial::valueChanged, midiDevices, &MidiDevices::SendMidiMessage);
    connect(&tpSource.cb, &QComboBox::currentIndexChanged, midiDevices, &MidiDevices::SendMidiMessage);

    // matrix sources
    connect(&matrixSource1, &comboBoxLabel::currentIndexChanged, midiDevices, &MidiDevices::updateMatrixSource);
    connect(&matrixSource2, &comboBoxLabel::currentIndexChanged, midiDevices, &MidiDevices::updateMatrixSource);
    connect(&matrixSource3, &comboBoxLabel::currentIndexChanged, midiDevices, &MidiDevices::updateMatrixSource);
    connect(&matrixSource4, &comboBoxLabel::currentIndexChanged, midiDevices, &MidiDevices::updateMatrixSource);
    connect(&matrixSource5, &comboBoxLabel::currentIndexChanged, midiDevices, &MidiDevices::updateMatrixSource);
    connect(&matrixSource6, &comboBoxLabel::currentIndexChanged, midiDevices, &MidiDevices::updateMatrixSource);
    connect(&matrixSource7, &comboBoxLabel::currentIndexChanged, midiDevices, &MidiDevices::updateMatrixSource);
    connect(&matrixSource8, &comboBoxLabel::currentIndexChanged, midiDevices, &MidiDevices::updateMatrixSource);
    connect(&matrixSource9, &comboBoxLabel::currentIndexChanged, midiDevices, &MidiDevices::updateMatrixSource);
    connect(&matrixSource10, &comboBoxLabel::currentIndexChanged, midiDevices, &MidiDevices::updateMatrixSource);

    // matrix amounts
    connect(&matrixAmount1, &QDial::valueChanged, midiDevices, &MidiDevices::updateMatrixAmount);
    connect(&matrixAmount2, &QDial::valueChanged, midiDevices, &MidiDevices::updateMatrixAmount);
    connect(&matrixAmount3, &QDial::valueChanged, midiDevices, &MidiDevices::updateMatrixAmount);
    connect(&matrixAmount4, &QDial::valueChanged, midiDevices, &MidiDevices::updateMatrixAmount);
    connect(&matrixAmount5, &QDial::valueChanged, midiDevices, &MidiDevices::updateMatrixAmount);
    connect(&matrixAmount6, &QDial::valueChanged, midiDevices, &MidiDevices::updateMatrixAmount);
    connect(&matrixAmount7, &QDial::valueChanged, midiDevices, &MidiDevices::updateMatrixAmount);
    connect(&matrixAmount8, &QDial::valueChanged, midiDevices, &MidiDevices::updateMatrixAmount);
    connect(&matrixAmount9, &QDial::valueChanged, midiDevices, &MidiDevices::updateMatrixAmount);
    connect(&matrixAmount10, &QDial::valueChanged, midiDevices, &MidiDevices::updateMatrixAmount);

    // matrix destinations
    connect(&matrixDestination1, &comboBoxLabel::currentIndexChanged, midiDevices, &MidiDevices::updateMatrixDestination);
    connect(&matrixDestination2, &comboBoxLabel::currentIndexChanged, midiDevices, &MidiDevices::updateMatrixDestination);
    connect(&matrixDestination3, &comboBoxLabel::currentIndexChanged, midiDevices, &MidiDevices::updateMatrixDestination);
    connect(&matrixDestination4, &comboBoxLabel::currentIndexChanged, midiDevices, &MidiDevices::updateMatrixDestination);
    connect(&matrixDestination5, &comboBoxLabel::currentIndexChanged, midiDevices, &MidiDevices::updateMatrixDestination);
    connect(&matrixDestination6, &comboBoxLabel::currentIndexChanged, midiDevices, &MidiDevices::updateMatrixDestination);
    connect(&matrixDestination7, &comboBoxLabel::currentIndexChanged, midiDevices, &MidiDevices::updateMatrixDestination);
    connect(&matrixDestination8, &comboBoxLabel::currentIndexChanged, midiDevices, &MidiDevices::updateMatrixDestination);
    connect(&matrixDestination9, &comboBoxLabel::currentIndexChanged, midiDevices, &MidiDevices::updateMatrixDestination);
    connect(&matrixDestination10, &comboBoxLabel::currentIndexChanged, midiDevices, &MidiDevices::updateMatrixDestination);
}

MainWindow::~MainWindow() {
    delete ui;
}

