#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <vector>

#include <QMainWindow>
#include <QLCDNumber>
#include <QDial>
#include <QHBoxLayout>
#include <QLabel>
#include <QComboBox>

#include "dial.h"
#include "mididevices.h"
#include "sectionBox.h"
#include "comboboxlabel.h"
#include "buttons.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
    Q_OBJECT

    public:
        MainWindow(MidiDevices *midiDevices);
        ~MainWindow();

        QWidget *mw;

        // controls:
        QComboBox testMidiStuff = QComboBox();

        // call constructors in .cpp file

        dial osc1Freq = dial("Freq", 0, 63, 0);
        dial osc1FreqByLfo1 = dial("Freq by LFO 1", 0, 63, 1);
        dial osc1Pw = dial("PW", 0, 63, 3);
        dial osc1PwByLfo2 = dial("PW by LFO2", 0, 63, 4);
        buttons osc1Waveform = buttons("Waveform", {"Pulse", "Wave"}, 6);
        dial osc1Ws = dial("WS (Saw - Tri)", 0, 63, 5);
        buttons osc1FixedMod1 = buttons("Fixed Mod", {"Pitch Bend", "Vibrato"}, 7);
        buttons osc1FixedMod2 = buttons("Fixed Mod", {"Portamento", "Click"}, 8);

        dial osc2Freq = dial("Freq", 0, 63, 10);
        dial osc2FreqByLfo1 = dial("Freq by LFO 1", 0, 63, 11);
        dial osc2Pw = dial("PW", 0, 63, 13);
        dial osc2PwByLfo2 = dial("PW by LFO2", 0, 63, 14);
        buttons osc2Waveform = buttons("Waveform", {"Pulse", "Wave", "Noise"}, 16);
        dial osc2Ws = dial("WS (Saw - Tri)", 0, 63, 15);
        buttons osc2FixedMod1 = buttons("Fixed Mod", {"Pitch Bend", "Vibrato"}, 17);
        buttons osc2FixedMod2 = buttons("Fixed Mod", {"Portamento", "Keyboard", "Click"}, 18);

        comboBoxLabel oscSync = comboBoxLabel("Sync", 2);
        dial detune = dial("Detune", 0, 63, 12);
        dial mix = dial("Mix (Osc1 - Osc2)", 0, 63, 20);
        dial ramp1Rate = dial("Rate", 0, 63, 40);
        comboBoxLabel ramp1Mode = comboBoxLabel("Mode", 41);
        dial ramp2Rate = dial("Rate", 0, 63, 42);
        comboBoxLabel ramp2Mode = comboBoxLabel("Mode", 43);
        comboBoxLabel keyboardMode = comboBoxLabel("Keyboard Mode", 48);
        dial portamentoRate = dial("Rate", 0, 63, 44);
        dial portamentoRateByVel = dial("Rate by Vel", 0, 63, 45);
        comboBoxLabel legatoMode = comboBoxLabel("Legato Mode", 46);
        buttons legato = buttons("Legato", {"Legato"}, 47);

        dial cutoff = dial("Cutoff", 0, 63, 21);
        dial resonance = dial("Resonance", 0, 63, 24);
        buttons filterFixedMod = buttons("Fixed Mod", {"Pitch Bend", "Vibrato"}, 25);
        dial cutoffByPressure = dial("CutByPressure", 0, 63, 23);
        dial cutoffByEnv1 = dial("CutByEnv1", 0, 63, 22);
        buttons cutoffByKeyboard = buttons("CutByKeyB", {"Portamento", "Keyboard"}, 26);
        dial fm = dial("FM", 0, 63, 30);
        dial fmByEnv3 = dial("FM by Env3", 0, 63, 31);
        dial fmByPressure = dial("FM by Pressure", 0, 63, 32);
        dial amp1Amount = dial("Amp1 Amnt", 0, 63, 27);
        dial amp1ByVel = dial("Amp1 by Vel", 0, 63, 28);
        dial amp2ByEnv2 = dial("Amp2 by Env2", 0, 63, 29);

        dial lfo1Speed = dial("Speed", 0, 63, 80);
        dial lfo1SpeedByPress = dial("SpeedByPress", 0, 63, 81);
        comboBoxLabel lfo1Wave = comboBoxLabel("Wave", 82);
        dial lfo1Amplitude = dial("Amplitude", 0, 63, 84);
        dial lfo1AmpByRamp1 = dial("AmpByRamp1", 0, 63, 85);
        dial lfo1RetrigPoint = dial("RetrigPoint", 0, 63, 83);
        comboBoxLabel lfo1Trigger = comboBoxLabel("Trigger", 86);
        buttons lfo1Lag = buttons("Lag", {"Lag"}, 87);
        comboBoxLabel lfo1SampledSource = comboBoxLabel("Sampled Source", 88);

        dial lfo2Speed = dial("Speed", 0, 63, 90);
        dial lfo2SpeedByPress = dial("SpeedByKB", 0, 63, 91);
        comboBoxLabel lfo2Wave = comboBoxLabel("Wave", 92);
        dial lfo2Amplitude = dial("Amplitude", 0, 63, 94);
        dial lfo2AmpByRamp2 = dial("AmpByRamp2", 0, 63, 95);
        dial lfo2RetrigPoint = dial("RetrigPoint", 0, 63, 93);
        comboBoxLabel lfo2Trigger = comboBoxLabel("Trigger", 96);
        buttons lfo2Lag = buttons("Lag", {"Lag"}, 97);
        comboBoxLabel lfo2SampledSource = comboBoxLabel("Sampled Source", 98);

        dial env1Delay = dial("Delay", 0, 63, 50);
        dial env1Attack = dial("Attack", 0, 63, 51);
        dial env1Decay = dial("Decay", 0, 63, 52);
        dial env1Sustain = dial("Sustain", 0, 63, 53);
        dial env1Release  = dial("Release", 0, 63, 54);
        dial env1Amplitude = dial("Amplitude", 0, 63, 55);
        dial env1AmpByVel = dial("Amp by Vel", 0, 63, 56);
        buttons env1TriggerMode = buttons("Trigger Mode", {"Reset", "Multi", "External"}, 57);
        buttons env1Mode = buttons("Mode", {"DADR", "Freerun"}, 58);
        buttons env1LfoTriggerMode = buttons("LFO Trigger Mode", {"Gated", "LFO Trig"}, 59);

        dial env2Delay = dial("Delay", 0, 63, 60);
        dial env2Attack = dial("Attack", 0, 63, 61);
        dial env2Decay = dial("Decay", 0, 63, 62);
        dial env2Sustain = dial("Sustain", 0, 63, 63);
        dial env2Release  = dial("Release", 0, 63, 64);
        dial env2Amplitude = dial("Amplitude", 0, 63, 65);
        dial env2AmpByVel = dial("Amp by Vel", 0, 63, 66);
        buttons env2TriggerMode = buttons("Trigger Mode", {"Reset", "Multi", "External"}, 67);
        buttons env2Mode = buttons("Mode", {"DADR", "Freerun"}, 68);
        buttons env2LfoTriggerMode = buttons("LFO Trigger Mode", {"Gated", "LFO Trig"}, 69);

        dial env3Delay = dial("Delay", 0, 63, 70);
        dial env3Attack = dial("Attack", 0, 63, 71);
        dial env3Decay = dial("Decay", 0, 63, 72);
        dial env3Sustain = dial("Sustain", 0, 63, 73);
        dial env3Release  = dial("Release", 0, 63, 74);
        dial env3Amplitude = dial("Amplitude", 0, 63, 75);
        dial env3AmpByVel = dial("Amp by Vel", 0, 63, 76);
        buttons env3TriggerMode = buttons("Trigger Mode", {"Reset", "Multi", "External"}, 77);
        buttons env3Mode = buttons("Mode", {"DADR", "Freerun"}, 78);
        buttons env3LfoTriggerMode = buttons("LFO Trigger Mode", {"Gated", "LFO Trig"}, 79);

        dial tp1 = dial("TP 1", 0, 63, 34);
        dial tp2 = dial("TP 2", 0, 63, 35);
        dial tp3 = dial("TP 3", 0, 63, 36);
        dial tp4 = dial("TP 4", 0, 63, 37);
        dial tp5 = dial("TP 5", 0, 63, 38);
        comboBoxLabel tpSource = comboBoxLabel("Source", 38);

        comboBoxLabel matrixSource1 = comboBoxLabel("Source", 0);
        dial matrixAmount1 = dial("Amount", -63, 63, 0);
        comboBoxLabel matrixDestination1 = comboBoxLabel("Destination", 0);

        comboBoxLabel matrixSource2 = comboBoxLabel("Source", 1);
        dial matrixAmount2 = dial("Amount", -63, 63, 1);
        comboBoxLabel matrixDestination2 = comboBoxLabel("Destination", 1);

        comboBoxLabel matrixSource3 = comboBoxLabel("Source", 2);
        dial matrixAmount3 = dial("Amount", -63, 63, 2);
        comboBoxLabel matrixDestination3 = comboBoxLabel("Destination", 2);

        comboBoxLabel matrixSource4 = comboBoxLabel("Source", 3);
        dial matrixAmount4 = dial("Amount", -63, 63, 3);
        comboBoxLabel matrixDestination4 = comboBoxLabel("Destination", 3);

        comboBoxLabel matrixSource5 = comboBoxLabel("Source", 4);
        dial matrixAmount5 = dial("Amount", -63, 63, 4);
        comboBoxLabel matrixDestination5 = comboBoxLabel("Destination", 4);

        comboBoxLabel matrixSource6 = comboBoxLabel("Source", 5);
        dial matrixAmount6 = dial("Amount", -63, 63, 5);
        comboBoxLabel matrixDestination6 = comboBoxLabel("Destination", 5);

        comboBoxLabel matrixSource7 = comboBoxLabel("Source", 6);
        dial matrixAmount7 = dial("Amount", -63, 63, 6);
        comboBoxLabel matrixDestination7 = comboBoxLabel("Destination", 6);

        comboBoxLabel matrixSource8 = comboBoxLabel("Source", 7);
        dial matrixAmount8 = dial("Amount", -63, 63, 7);
        comboBoxLabel matrixDestination8 = comboBoxLabel("Destination", 7);

        comboBoxLabel matrixSource9 = comboBoxLabel("Source", 8);
        dial matrixAmount9 = dial("Amount", -63, 63, 8);
        comboBoxLabel matrixDestination9 = comboBoxLabel("Destination", 8);

        comboBoxLabel matrixSource10 = comboBoxLabel("Source", 9);
        dial matrixAmount10 = dial("Amount", -63, 63, 9);
        comboBoxLabel matrixDestination10 = comboBoxLabel("Destination", 9);

    private:
        Ui::MainWindow *ui;
        MidiDevices *midiDevices;

        // synth sections:
        // *** can we use static allocation instead??
        sectionBox *osc1 = new sectionBox(4); // oscillator 1
        sectionBox *osc2 = new sectionBox(4); // oscillator 2
        sectionBox *filterAmp = new sectionBox(3); // filter + amp
        sectionBox *lfo1 = new sectionBox(3); // LFO 1
        sectionBox *lfo2 = new sectionBox(3); // LFO 2
        sectionBox *env1 = new sectionBox(5); // envelope 1
        sectionBox *env2 = new sectionBox(5); // envelope 2
        sectionBox *env3 = new sectionBox(5); // envelope 3
        sectionBox *tp = new sectionBox(5); // tracking point
        sectionBox *misc = new sectionBox(4); // miscellaneous
        sectionBox *matrix = new sectionBox(3); // modulation matrix

        QStringList oscSyncCodes = {"No", "Soft", "Medium", "Hard"};

        QStringList rampModeCodes = {"Single", "Multi", "ExtTrig", "ExtGate"};

        QStringList keyboardModeCodes = {"Reassign", "Rotate", "Unison", "Rob"};

        QStringList legatoModeCodes = {"CSpeed", "CTime", "Expo"};

        QStringList lfoWaveCodes = {"Triangle", "Up Saw", "Down Saw",
            "Square", "Random", "Noise", "SampledMod"};

        QStringList lfoTriggerCodes = {"No", "Single", "Multi", "External"};

        QStringList matrixSourceCodes = {"*Unused", "Envelope 1",
            "Envelope 2", "Envelope 3", "LFO 1", "LFO 2", "Vibrato",
            "Ramp 1", "Ramp 2", "Keyboard", "Portamento",
            "Tracking Gen", "KB Gate", "Velocity",
            "Release Vel", "Pressure", "Pedal 1", "Pedal 2",
            "Lever 1", "Lever 2", "Lever 3"};

        QStringList matrixDestinationCodes = {"*Unused", "DCO 1 Freq",
            "DCO 1 Pulse Width", "DCO 1 Wave Shape", "DCO 2 Freq",
            "DCO 2 Pulse Width", "DCO 2 Wave Shape", "Mix Level",
            "VCF FM Amount", "VCF Frequency", "VCF Resonance", "VCA 1 Level",
            "VCA 2 Level", "Envelope 1 Delay", "Envelope 1 Attack",
            "Envelope 1 Decay", "Envelope 1 Release", "Envelope 1 Amp",
            "Envelope 2 Delay", "Envelope 2 Attack", "Envelope 2 Decay",
            "Envelope 2 Release", "Envelope 2 Amp", "Envelope 3 Delay",
            "Envelope 3 Attack", "Envelope 3 Decay", "Envelope 3 Release",
            "Envelope 3 Amp", "LFO 1 Speed", "LFO 1 Amp",
            "LFO 2 Speed", "LFO 2 Amp", "Portamento Time"};

};

#endif // MAINWINDOW_H

