#ifndef SECTIONBOX_H
#define SECTIONBOX_H

#include <QFrame>
#include <QLayout>


// an entire section of the synth - e.g. Osc1, Filter, Env1
// contains multiple controls organized in columns

// label:
// maybe divide it up in 2 sections
// first section is label only - takes up an entire row but very short vertically
// second row is everything else that's already here - the existing layout and widgets

class sectionBox : public QFrame {
    Q_OBJECT

    public:
        sectionBox(int numColumns);
        void addWidget(QWidget *qw);

    protected:

    private:
        QGridLayout layout = QGridLayout();
        int numColumns, widgetIndex = 0;

};

#endif // SECTIONBOX_H
