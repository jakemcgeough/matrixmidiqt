#ifndef MIDIDEVICES_H
#define MIDIDEVICES_H

#pragma comment(lib, "windowsapp")

#include <winrt/Windows.Foundation.h>
#include <Windows.h>
#include <winrt/Windows.Devices.Enumeration.h>

#include <winrt/Windows.Foundation.Collections.h>
#include <winrt/Windows.Storage.Streams.h>
#include <winrt/Windows.UI.Core.h>
#include <winrt/Windows.Devices.Midi.h>

#include <QString>
#include <QList>
#include <QStringList>
#include <QObject>

#include <iostream>
#include <vector>

#include "dial.h"
#include "comboboxlabel.h"

using namespace std;
using namespace winrt;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::Storage::Streams;
using namespace Windows::Devices::Enumeration;
using namespace Windows::Devices::Midi;

// separate class for sending midi messages?? pass this pointer to that class
// or include that stuff in this class??

class MidiDevices : public QObject {
    Q_OBJECT

    public:
        MidiDevices();
        void SelectOutDevice(int devIndex);
        size_t GetOutDeviceCount();
        QStringList GetOutDeviceNames();
        IMidiOutPort *GetCurrentOutDevice();

    public slots:
        void SendMidiMessage(int value);

        void updateMatrixSource(int value);
        void updateMatrixAmount(int value);
        void updateMatrixDestination(int value);

    private:
        void SendMatrixMessage(int param);

        QStringList outDeviceNames;
//        vector<string> outDevicesOrganized;
        vector<hstring> outDeviceIds;

        vector<IMidiOutPort> outPorts; // do we use this?
        IMidiOutPort currentOutPort;
        hstring currentOutId;

        size_t outDeviceCount = 0;

        void FindOutPorts();

        vector<int> matrixSources {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        vector<int> matrixAmounts {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        vector<int> matrixDestinations {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
};

#endif // MIDIDEVICES_H

