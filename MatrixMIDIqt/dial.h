#ifndef DIAL_H
#define DIAL_H

#include <QDial>
#include <QMouseEvent>
#include <QString>
#include <QSharedPointer>

#include <QPainter>
#include <QColor>
#include <QLabel>
#include <QRectF>
#include <QPen>
#include <QResizeEvent>

//#include "mididevices.h"

class dial : public QDial {
    Q_OBJECT

    Q_PROPERTY(QString arcColor READ GetArcColor WRITE SetArcColor)

    Q_PROPERTY(double arcWidth READ GetArcWidth WRITE SetArcWidth)

    public:
        explicit dial(QWidget* parent = nullptr);
//        dial(const QString &title, QWidget *parent, int min, int max);
        dial(const QString title, int min, int max, int param);
        ~dial(); // necessary?

        int GetValue();

        QString GetArcColor() const;
        double GetStartAngle() const;
        double GetMaximumAngle() const;
        double GetArcWidth() const;
        QString GetText() const;

        int GetParam() const;

        void SetArcColor(const QString& color);
        void SetStartAngle(double angle);
        void SetMaximumAngle(double angle);
        void SetArcWidth(double px);
        void SetText(const QString& text);

    private slots:
        void updateValue();

    protected:
        virtual void mousePressEvent(QMouseEvent *event) override;
        virtual void mouseReleaseEvent(QMouseEvent *event) override;
        virtual void mouseMoveEvent(QMouseEvent *event) override;

    private:
        bool   dragging;
        QPoint mousePressPoint;
        int    baseValue = 1;
        double  scaleFactor = 0.35;

        virtual void paintEvent(QPaintEvent*) override;

        virtual void resizeEvent(QResizeEvent* event) override;
        double maximumAngleSpan;
        double startAngle;
        double arcWidth;
        double angleSpan;
        QString valueString;
        QString text;
        int param;
        QSharedPointer<QRectF> arcRect;
        QSharedPointer<QRectF> valueRect;
        QSharedPointer<QRectF> textRect;
        QSharedPointer<QColor> arcColor;
        QSharedPointer<QPen> arcPen;
};




#endif // DIAL_H
