#ifndef BUTTONS_H
#define BUTTONS_H

#include <vector>
#include <map>

#include <QWidget>
#include <QPushButton>
#include <QCheckBox>
#include <QLabel>
#include <QLayout>

#include "pushbutton.h"

// label + vector of buttons

class buttons : public QWidget {
    Q_OBJECT

    public:
        buttons(QString title, std::vector<QString> bNames, int param);
        pushButton *GetButtonByName(QString bName);
        int GetParam() const;

    public slots:
        void PressButton(int currentBit);

    signals:
        void updateValue(int val);

    private:
        QLabel label = QLabel();
        QVBoxLayout layout = QVBoxLayout();

        std::map<QString, pushButton*> bNamePointers;

        size_t numBits;
        int value;

        int param;
};

#endif // BUTTONS_H
