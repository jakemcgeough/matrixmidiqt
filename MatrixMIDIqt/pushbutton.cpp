#include "pushbutton.h"

pushButton::pushButton(const QString text, int bit)
    : QPushButton(text),
      bit(bit)
{

    connect(this, &QPushButton::clicked, this, &pushButton::ReceiveButtonPress);
}

int pushButton::GetBit() {
    return bit;
}

void pushButton::ReceiveButtonPress(bool clicked) {
    emit PressButton(bit);
}
