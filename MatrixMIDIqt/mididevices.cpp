#include "mididevices.h"

MidiDevices::MidiDevices() {
    // find all MIDI out devices:
    FindOutPorts();

    for (auto i : outDeviceNames) {
        std::cout << i.toStdString() << std::endl;
    }

    // default to first device if any devices found:
    if (outDeviceCount >= 1) {
        SelectOutDevice(0);
    }

//    cout << matrixSources.size() << endl;
//    cout << matrixAmounts.size() << endl;
//    cout << matrixDestinations.size() << endl;

}

void MidiDevices::SelectOutDevice(int devIndex) {
    size_t ind;
    if (devIndex >= 0) {
        ind = (size_t)devIndex;
    }
    if (ind <= outDeviceCount) {
        currentOutId = outDeviceIds[ind];
        IAsyncOperation<IMidiOutPort> currentPortAsync =
                MidiOutPort::FromIdAsync(currentOutId);

        int currentStatus = 0;
        while (!(currentStatus = (int)currentPortAsync.Status())) {}
        currentOutPort = currentPortAsync.GetResults();
    }
}

void MidiDevices::FindOutPorts() {
    hstring midiOutString = MidiOutPort::GetDeviceSelector();
    IAsyncOperation<DeviceInformationCollection> midiOutCollection =
            DeviceInformation::FindAllAsync(midiOutString);

// get input devices - later:
//    hstring midiInString = MidiInPort::GetDeviceSelector();
//    IAsyncOperation<DeviceInformationCollection> midiInCollection =
//            DeviceInformation::FindAllAsync(midiInString);

    int currentStatus = 0;
    while (!(currentStatus = (int)midiOutCollection.Status())) {}

    DeviceInformationCollection moc = midiOutCollection.GetResults();
    IIterator<DeviceInformation> itMoc = moc.First();

    // store names and IDs of MIDI out devices:
    int count = moc.Size();
    while (count--) {
        outDeviceNames.append(QString::fromStdString(to_string(itMoc.Current().Name())));
        currentOutId = itMoc.Current().Id();
        outDeviceIds.push_back(currentOutId);
        itMoc.MoveNext();
    }

    outDeviceCount = outDeviceNames.size();

    // maybe use this later - find devices dynamically
    // Windows::Devices::Enumeration::DeviceWatcher dw = DeviceInformation::CreateWatcher();


}

size_t MidiDevices::GetOutDeviceCount() {
    return outDeviceCount;
}

QStringList MidiDevices::GetOutDeviceNames() {
    return outDeviceNames;
}

IMidiOutPort *MidiDevices::GetCurrentOutDevice() {
    return &currentOutPort;
}


void MidiDevices::SendMidiMessage(int value) {
    dial *sender = static_cast<dial*>(QObject::sender()); // is this ok?
    int param = sender->GetParam();

    BYTE paramByte = static_cast<unsigned char>(param);
    BYTE valueByte = static_cast<unsigned char>(value);

    if (param == 12 && value < 0) {
         value += 128;
     }
     else if (value < 0) {
         value += 128;
     }

     BYTE toSend[1024] = { 0xF0, 0x10, 0x06, 0x06, paramByte, valueByte, 0xF7 };
     DataWriter dw;
     dw.WriteBytes(toSend);
     IBuffer buf = dw.DetachBuffer();
     MidiSystemExclusiveMessage sysex = MidiSystemExclusiveMessage(buf);
     currentOutPort.SendMessage(sysex);
}

void MidiDevices::updateMatrixSource(int value) {
    comboBoxLabel *sender = static_cast<comboBoxLabel*>(QObject::sender());
    int param = sender->GetParam();

//    cout << "param: " << param << endl;

    matrixSources[param] = value;
//    cout << "here 1" << endl;
    SendMatrixMessage(param);
}

void MidiDevices::updateMatrixAmount(int value) {
    dial *sender = static_cast<dial*>(QObject::sender());
    int param = sender->GetParam();

//    cout << "param: " << param << endl;

    matrixAmounts[param] = value;
//    cout << "here 2" << endl;
    SendMatrixMessage(param);
}

void MidiDevices::updateMatrixDestination(int value) {
    comboBoxLabel *sender = static_cast<comboBoxLabel*>(QObject::sender());
    int param = sender->GetParam();

//    cout << "param: " << param << endl;

    matrixDestinations[param] = value;
//    cout << "here 3" << endl;
    SendMatrixMessage(param);
}

void MidiDevices::SendMatrixMessage(int param) {

    if (!(matrixSources[param]) || !(matrixDestinations[param])) { // if either is 0, don't send
        return;
    }

    BYTE paramByte = static_cast<unsigned char>(param);
    BYTE sourceByte = static_cast<unsigned char>(matrixSources[param]);
    BYTE valueByte = static_cast<unsigned char>(matrixAmounts[param]);
    BYTE destByte = static_cast<unsigned char>(matrixDestinations[param]);

//    cout << "matrix message: " << param << " " << matrixSources[param] << " "
//            << matrixAmounts[param] << " " << matrixDestinations[param] << endl;

    // --------------------------
    // CHECK THE FORMATTING:
    // --------------------------
    BYTE toSend[1024] = { 0xF0, 0x10, 0x06, 0x0B, paramByte, sourceByte, valueByte, destByte, 0xF7 };
    DataWriter dw;
    dw.WriteBytes(toSend);
    IBuffer buf = dw.DetachBuffer();
    MidiSystemExclusiveMessage sysex = MidiSystemExclusiveMessage(buf);
    currentOutPort.SendMessage(sysex);
}





















