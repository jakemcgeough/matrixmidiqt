#ifndef COMBOBOXLABEL_H
#define COMBOBOXLABEL_H

#include <QWidget>
#include <QComboBox>
#include <QLabel>
#include <QString>
#include <QLayout>

class comboBoxLabel : public QWidget {
    Q_OBJECT

    public:
        comboBoxLabel(QString title, int param);
        void addItems(QStringList items);
        QComboBox cb = QComboBox();
        int GetParam() const;

    public slots:
        void changeIndex(int value);

    signals:
        void currentIndexChanged(int value);

    private:
        QLabel label = QLabel();
        QVBoxLayout layout = QVBoxLayout();
        int param;
};

#endif // COMBOBOXLABEL_H

