#ifndef PUSHBUTTON_H
#define PUSHBUTTON_H

#include <QPushButton>

class pushButton : public QPushButton {
    Q_OBJECT

    public:
        pushButton(const QString text, int bit);
        int GetBit();

    public slots:
        void ReceiveButtonPress(bool clicked);

    signals:
        void PressButton(int b);

    private:
        int bit;
};

#endif // PUSHBUTTON_H
